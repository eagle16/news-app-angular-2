import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-news',
  templateUrl: './form-news.component.html',
  styleUrls: ['./form-news.component.css']
})
export class FormNewsComponent {

  @Output() onCreateNews = new EventEmitter();
  @Output() closeDialog = new EventEmitter();

  newsForm: FormGroup;

  constructor() {
    this.newsForm = new FormGroup({
      "newCategory": new FormControl("", Validators.required),
      "newImage": new FormControl("", Validators.required),
      "newDescription": new FormControl("", Validators.required)
    })
  }

  dataForNew() {
    //Get data from FromNews and send it to main app
    let data = this.newsForm.value;
    this.onCreateNews.emit(data);
    //Hide dialog window
    this.closeForm()
    console.log(this.newsForm.value);
  }

  closeForm(){
    this.closeDialog.emit();
  }
}
