export class Init {
    load() {
        if (!localStorage.getItem('news')) {
            let allNews = [
                {
                    "id": "1",
                    "img": "https://weddingbroker.files.wordpress.com/2016/03/wedding-broker-primavera-2.jpg?w=680",
                    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                    "category": "health",
                    "liked": "1"
                },
                {
                    "id": "2",
                    "img": "http://rcysl.com/wp-content/uploads/2017/02/Life-Images-HD-.jpg",
                    "description": "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                    "category": "life",
                    "liked": "0"
                },
                {
                    "id": "3",
                    "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Good_Food_Display_-_NCI_Visuals_Online.jpg/1200px-Good_Food_Display_-_NCI_Visuals_Online.jpg",
                    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                    "category": "food",
                    "liked": "0"
                }
            ]
            localStorage.setItem('news',JSON.stringify(allNews));
        }
    }
}