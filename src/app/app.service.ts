import { Injectable } from '@angular/core';
import { Init } from './init-data';

@Injectable()
export class AppService extends Init {
    constructor() {
        super();
        console.log('Service Inicializing...')
        this.load();
    }

    getData() {
        let allNews = this.parseData();
        return allNews;
    }

    addNews(newData) {
        let allNews = this.parseData();
        //Adding
        allNews.push(newData);
        //Set New 
        this.setData(allNews);
    }

    deleteNew(index) {
        let allNews = this.parseData();
        //Delete
        allNews.splice(index,1);
        //Set updated
        this.setData(allNews);
    }

    likedNew(index,val){
        let allNews = this.parseData();
        allNews[index].liked = val;
        this.setData(allNews);
    }

    parseData(){
        return JSON.parse(localStorage.getItem('news'));
    }

    setData(data){
        localStorage.setItem('news', JSON.stringify(data));
    }
}