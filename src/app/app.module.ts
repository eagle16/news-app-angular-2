import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule }   from '@angular/forms';

import { AppService } from './app.service';

import { CategoryFilter } from './category-filter.pipe';

import { AppComponent } from './app.component';

import { routes } from './app.routes';
import 'hammerjs';

import { HealthComponent } from './health/health.component';
import { LifeComponent } from './life/life.component';
import { FoodComponent } from './food/food.component';
import { FormNewsComponent } from './form-news/form-news.component';

@NgModule({
  declarations: [
    AppComponent,
    CategoryFilter,
    HealthComponent,
    LifeComponent,
    FoodComponent,
    FormNewsComponent
  ],
  imports: [
    BrowserModule, 
    HttpModule,
    RouterModule.forRoot(routes),
    MaterialModule,
    BrowserAnimationsModule,
    ReactiveFormsModule 
  ],
  providers: [AppService],
  bootstrap: [AppComponent],
  entryComponents: [FormNewsComponent]
})
export class AppModule { }
