import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css']
})
export class FoodComponent implements OnInit {

  constructor() { }

  @Input() foodNews;
  @Output() onDelete = new EventEmitter();
  @Output() onLike = new EventEmitter();

  foodFilter = { 'category': 'food' };

  ngOnInit() {

  }

  deleteMe(id) {
    this.onDelete.emit(id);
  }

  likeMe(id) {
    this.onLike.emit(id);
  }
}
