import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from './app.service';
import { FormNewsComponent } from './form-news/form-news.component'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  newsList = [];
  public showDialog: boolean = false;
  liked: boolean = true;

  constructor(private newsService: AppService) { }

  ngOnInit() {
    this.newsList = this.newsService.getData();
    console.log(this.newsList);

  }

  openDialog() {
    this.showDialog = true;
  }
  closeDialog() {
    this.showDialog = false;
  }

  onCreateNews(data) {
    let newItem = {
      "id": this.newsList.length + 1,
      "img": data.newImage,
      "description": data.newDescription,
      "category": data.newCategory,
      "liked": "1"
    }
    this.newsList.push(newItem);

    this.newsService.addNews(newItem);

    console.log(this.newsList);
  }

  onDelete(id) {
    let indexNew: number;
    this.newsList.forEach((element, index) => {
      if (element.id == id) {
        indexNew = index;
        this.newsList.splice(index, 1);
      }
    });

    this.newsService.deleteNew(indexNew);
  }

  onLike(id) {
    this.newsList.forEach((element, index) => {
      if (element.id == id) {
        if (this.newsList[index].liked) {
            this.newsList[index].liked = false
            this.newsService.likedNew(index, false);
        }
        else {
          this.newsList[index].liked = true;
          this.newsService.likedNew(index, true);
        }  
      }
    });
  }
}
