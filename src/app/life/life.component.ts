import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-life',
  templateUrl: './life.component.html',
  styleUrls: ['./life.component.css']
})
export class LifeComponent implements OnInit {

  constructor() { }

  @Input() lifeNews;
  @Output() onDelete = new EventEmitter();
  @Output() onLike = new EventEmitter();

  lifeFilter = { 'category': 'life' };

  ngOnInit() {

  }

  deleteMe(id) {
    this.onDelete.emit(id);
  }

  likeMe(id) {
    this.onLike.emit(id);
  }
}
