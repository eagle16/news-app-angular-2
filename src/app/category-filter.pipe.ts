import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'categoryfilter',
    pure: false
})
export class CategoryFilter implements PipeTransform {
    transform(items: any[], filter): any {
        if (!items || !filter) {
            return items;
        }
       
        return items.filter(item => item.category.indexOf(filter.category) !== -1);
    }
}