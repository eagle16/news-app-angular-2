import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-health',
  templateUrl: './health.component.html',
  styleUrls: ['./health.component.css']
})
export class HealthComponent implements OnInit {

  @Input() healthNews;
  @Output() onDelete = new EventEmitter();
  @Output() onLike = new EventEmitter();

  healthFilter = {'category':'health'};

  constructor(private newsService: AppService) { }

  ngOnInit() {
    console.log(this.healthNews);
  }

  deleteMe(id){
    this.onDelete.emit(id);
  } 
  
  likeMe(id){
    this.onLike.emit(id);
  }
}
